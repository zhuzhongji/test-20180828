package com.nutstore.web.framework.util;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * @author zhuzhongji  
 * @date 2018年8月28日 下午4:40:59
 */
public interface IJson {
	public JSONObject toJSONObject();
}
