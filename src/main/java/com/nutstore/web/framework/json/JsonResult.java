package com.nutstore.web.framework.json;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * @author zhuzhongji  
 * @date 2018年8月28日 下午3:35:43
 */
public interface JsonResult {
	public static final int CODE_SUCCESS = 100;
	public static final int CODE_PARMETER_ERROR = 0;
	public static final int CODE_SERVER_ERROR = -1;
	public static final int CODE_SECURITY_ERROR = -2;	//安全性错误
	
	public JSONObject toJSONObject();
	
}
