package com.nutstore.web.framework.json;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 * @author zhuzhongji  
 * @date 2018年8月28日 下午3:35:12
 */
public class CodeResult implements JsonResult {
	JSONObject jsonObject;
	
	
	public CodeResult(){
		jsonObject = new JSONObject();
		jsonObject.put("code", -1);
	}
	public CodeResult(int code){
		jsonObject = new JSONObject();
		jsonObject.put("code", code);
	}
	
	public CodeResult(int code, Object result) {
		jsonObject = new JSONObject();
		jsonObject.put("code", code);
		if (result != null) {
			jsonObject.put("result", result);
		}
	}
	
	@Override
	public JSONObject toJSONObject() {
		return jsonObject;
	}

	public int getCode() {
		return jsonObject.getIntValue("code");
	}

	public void setCode(int code) {
		jsonObject.put("code", code);
	}

	public Object getResult() {
		return jsonObject.get("result");
	}

	public void setResult(Object result) {
		jsonObject.put("result", result);
	}

	public void put(String key, Object value) {
		if(value == null) {
			jsonObject.remove(key);
		} else {
			jsonObject.put(key, JSON.toJSON(value));
		}
	}
}
