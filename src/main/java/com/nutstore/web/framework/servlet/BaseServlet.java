package com.nutstore.web.framework.servlet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nutstore.web.framework.json.CodeResult;
import com.nutstore.web.framework.json.JsonResult;


/**
 * 
 * @author zhuzhongji  
 * @date 2018年8月28日 下午3:43:43
 */
@SuppressWarnings("serial")
public class BaseServlet extends HttpServlet {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	public static final int PAGE_LIMIT = 10;
	public final static String APP = "app";
	@SuppressWarnings("unused")
	private static final DateFormat DATE_FORMAT;
	static {
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8:00"));
		DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	}
	
	protected Date currentTime;
	protected CodeResult jsonResult;
	@SuppressWarnings("rawtypes")
	protected List list;
	protected Object result;
	
	protected int page = 1;
	
	protected int code = 100;
	protected String id;
	
	protected String content;
	protected int type = 0;
	protected long userId;
	protected String username;
	protected String postId;
	protected long topicId;
	protected long aimId = 0;
	protected String videoId;
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Date getCurrentTime() {
		if (currentTime == null) {
			return new Date();
		}
		return currentTime;
	}

	public HttpSession getSession() {
		return this.getRequest().getSession();
	}
	
	public JsonResult getJsonResult() {
		return jsonResult;
	}

	public HttpServletResponse getResponse() {
	        return this.getResponse();
	 }
	 
	public HttpServletRequest getRequest() {
	        return this.getRequest();  
	    }

	@SuppressWarnings("rawtypes")
	public List getList() {
		return list;
	}
	
	@SuppressWarnings("rawtypes")
	public void setList(List list) {
		this.list = list;
	}

	public Object getResult() {
		return result;
	}
	
	public void setResult(Object result) {
		this.result = result;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getString(String parmeter) {
		return this.getRequest().getParameter(parmeter);
	}
	
	public int getInt(String parmeter) {
		String value = this.getRequest().getParameter(parmeter);
		if(value == null) return 0;
		return Integer.parseInt(value);
	}
	
	public double getDouble(String parmeter) {
		String value = this.getRequest().getParameter(parmeter);
		if(value == null) return 0;
		return Double.parseDouble(value);
	}
	
	public long getLong(String parmeter) {
		String value = this.getRequest().getParameter(parmeter);
		if(value == null) return 0;
		return Long.parseLong(value);
	}
	
	public float getFloat(String parmeter) {
		String value = this.getRequest().getParameter(parmeter);
		if(value == null) return 0;
		return Float.parseFloat(value);
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public void setAimId(long aimId) {
		this.aimId = aimId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	
}
