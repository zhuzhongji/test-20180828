package com.nutstore.web.framework.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author zhuzhongji  
 * @date 2018年8月28日 下午3:43:58
 */
@SuppressWarnings("serial")
public class HealthcheckServlet extends BaseServlet {

	@Override
	  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	      resp.setStatus(200);
	      resp.getWriter().print("Healthcheck test!!!!");
	  }
}
