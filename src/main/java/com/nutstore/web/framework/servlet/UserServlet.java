package com.nutstore.web.framework.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.nutstore.web.framework.json.CodeResult;

/**
 * 
 * @author zhuzhongji  
 * @date 2018年8月28日 下午4:41:10
 */
@SuppressWarnings("serial")
public class UserServlet extends BaseServlet {

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    
	    list = new ArrayList<String>();
		list.add("aa");
		list.add("bb");
		list.add("cc");
		jsonResult = new CodeResult();
		jsonResult.setCode(CodeResult.CODE_SUCCESS);
		jsonResult.setResult(list);

	    resp.getWriter().print(JSON.toJSONString(jsonResult));
	}
}
