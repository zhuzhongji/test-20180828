package com.nutstore.web.framework.server;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.Date;
import java.util.TimeZone;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nutstore.web.framework.servlet.HealthcheckServlet;
import com.nutstore.web.framework.servlet.IndexServlet;
import com.nutstore.web.framework.servlet.UserServlet;
import com.nutstore.web.framework.util.Configure;

public class JettyServer {

	private static final Logger logger = LoggerFactory.getLogger(JettyServer.class);
	public static int ONLINE_COUNT = 0;
	public static Date serverStartTime = new Date();
	static {
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8:00"));
	}
	

	private boolean running = false;
	private int port;
	@SuppressWarnings("unused")
	private String context;
	@SuppressWarnings("unused")
	private int scanIntervalSeconds;
	private Server server;

	
	public JettyServer() {
	}

	public void initialize(String classpath) {
		//
	}

	public void start(String baseDir) throws Throwable {
		port = Configure.getInteger("jetty.http.port", 8089);
		context = Configure.get("jetty.webapp.context", "/");
		scanIntervalSeconds = Configure.getInteger("jetty.scanIntervalSeconds", 0);
		if (!portAvailable(port)) {
			throw new IllegalStateException("port: " + port + " already in use!");
		}

		System.setProperty("org.eclipse.jetty.util.URI.charset", "UTF-8");
		System.setProperty("org.eclipse.jetty.util.log.class", "org.eclipse.jetty.util.log.Slf4jLog");
		QueuedThreadPool threadPool = new QueuedThreadPool(200, 20, 120000);
		server = new Server(threadPool);
		server.setStopAtShutdown(true);

		ServerConnector webConnector = new ServerConnector(server);
		webConnector.setPort(port);
		server.setConnectors(new Connector[] { webConnector });

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.addServlet(new ServletHolder(new HealthcheckServlet()),"/healthz"); 		//http://127.0.0.1:8088/healthz 
        context.addServlet(new ServletHolder(new IndexServlet()),"/*"); 					//http://127.0.0.1:8088/ 
        context.addServlet(new ServletHolder(new UserServlet()), "/user"); 					//http://127.0.0.1:8088/user 

		HandlerCollection hc = new HandlerCollection();
		hc.setHandlers(new Handler[] { context });

		logger.info("Context path:" + context.getContextPath());

		server.setHandler(hc);

		long ts = System.currentTimeMillis();
		server.start();

		ts = System.currentTimeMillis() - ts;
		logger.info("Jetty Server started: " + String.format("%.2f sec", ts / 1000d) + ", listen port " + port);
		logger.info("Server starting success!");
		running = true;
	}

	private static boolean portAvailable(int port) {
		if (port <= 0) {
			throw new IllegalArgumentException("Invalid start port: " + port);
		}

		ServerSocket serverSocket = null;
		DatagramSocket datagramSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			serverSocket.setReuseAddress(true);
			datagramSocket = new DatagramSocket(port);
			datagramSocket.setReuseAddress(true);
			return true;
		} catch (IOException e) {
		} finally {
			if (datagramSocket != null) {
				datagramSocket.close();
			}
			if (serverSocket != null)
				try {
					serverSocket.close();
				} catch (IOException e) {
				}
		}
		return false;
	}

	public void stop() {
		Bootstrap.runningStat = 0;
		running = false;
		try {
			this.server.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Server stop finish");
		System.gc();
		System.exit(0);
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
}
