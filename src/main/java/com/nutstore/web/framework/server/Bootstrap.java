package com.nutstore.web.framework.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.AccessControlException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
//import com.nutstore.web.framework.org.json.JSONObject;
import com.nutstore.web.framework.util.Configure;

public class Bootstrap {

	public static final String START = "start";
	public static final String STOP = "stop";
	public static final String RELOAD = "reload";

	public static int runningStat = 0; // 0 停止状态，1运行状态 -1 即将停止状态
	private static Bootstrap bootstrap;
	private JettyServer server;
	private Random random = null;
	private int port;

	public static void main(String[] args) {
		Bootstrap boot = new Bootstrap();
		try {
			boot.init();
			
			String command = START;
			if (args.length > 0) {
				command = args[args.length - 1];
			}
			
			if (command.equals(START)) {
				System.out.println("Server start...");
				bootstrap = boot;
				boot.startServer();
				System.out.println("Server start up @ " + new Date().toString());
				bootstrap.await();
				bootstrap.stopServer();
			} else if (command.equals(STOP)) {
				System.out.println("Server stop...");
				boot.sendCommand(STOP);
				System.exit(0);
			} else if (command.equals(RELOAD)) {
				JSONObject jo = new JSONObject(); jo.put("msgId", 334);
				jo.put("requestType", "remote");
				boot.sendCommand(jo.toString()); System.exit(0);
			} else {
				System.err.println("Bootstrap.await: Invalid command '" + command.toString() + "' received");
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	private void init() {
		String classPath = getClassDir();
		String configFile = classPath + "server.properties";
		Map<String, String> vars = new HashMap<String, String>();
		vars.put("home", classPath);
		Configure.initialize(configFile, vars);

		port = Configure.getInteger("severCommand.port", 3088);
	}

	private void startServer() {
		server = new JettyServer();
		server.initialize(getClassDir());
		try {
			server.start(getBaseDir());
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void stopServer() throws Exception {
		server.stop();
	}

	private void sendCommand(String command) {
		try {
			Socket socket = new Socket("127.0.0.1", port);
			OutputStream stream = socket.getOutputStream();
			for (int i = 0; i < command.length(); i++) {
				stream.write(command.charAt(i));
			}
			stream.flush();
			stream.close();
			socket.close();
		} catch (IOException e) {
			System.err.println("IOException occurred: " + e.getMessage());
		}
	}

	public void await() {
		// Set up a server socket to wait on
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port, 1, InetAddress.getByName("127.0.0.1"));
		} catch (IOException e) {
			System.err.println("Bootstrap.await: create[" + port + "]: " + e.getMessage());
			System.exit(1);
		}

		// Loop waiting for a connection and a valid command
		while (true) {
			// Wait for the next connection
			Socket socket = null;
			InputStream stream = null;
			try {
				socket = serverSocket.accept();
				socket.setSoTimeout(10 * 1000); // Ten seconds
				stream = socket.getInputStream();
			} catch (AccessControlException ace) {
				System.err.println("StandardServer.accept security exception: " + ace.getMessage());
				continue;
			} catch (IOException e) {
				System.err.println("StandardServer.await: accept: " + e.getMessage());
				System.exit(1);
			}

			// Read a set of characters from the socket
			StringBuffer command = new StringBuffer();
			int expected = 1024; // Cut off to avoid DoS attack
			while (expected < STOP.length()) {
				if (random == null) {
					random = new Random(System.currentTimeMillis());
				}
				expected += (random.nextInt() % 1024);
			}
			while (expected > 0) {
				int ch = -1;
				try {
					ch = stream.read();
				} catch (IOException e) {
					System.err.println("Bootstrap.await: read: " + e.getMessage());
					ch = -1;
				}
				if (ch < 32) // Control character or EOF terminates loop
					break;
				command.append((char) ch);
				expected--;
			}

			// Close the socket now that we are done with it
			try {
				socket.close();
			} catch (IOException e) {
			}

			if (command.toString().equals(STOP)) {
				break;
			} else if (command.toString().equals(RELOAD)) {
			} else {
				System.err.println("Bootstrap.await: Invalid command '" + command.toString() + "' received");
			}
		}

		try {
			serverSocket.close();
		} catch (IOException e) {
			
		}

	}

	public static Bootstrap getBootstrap() {
		return bootstrap;
	}

	public static void setBootstrap(Bootstrap bootstrap) {
		Bootstrap.bootstrap = bootstrap;
	}

	private static String getBaseDir() {
		String classPath = getClassDir();
		String baseDir = classPath.substring(0, classPath.indexOf("classes"));
		return baseDir;
	}

	private static String getClassDir() {
		String classPath = Bootstrap.class.getResource("/").getPath();
		return classPath;
	}
}
